package arkontrol.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ResultSetParser {

    public static int getInt(ResultSet resultSet, String columnName) {
        int result = -1;

        try {
            while(resultSet.next()) {
                result = resultSet.getInt(columnName);
            }
        } catch (SQLException e) {
            e.getMessage();
            e.printStackTrace();
        }

        return result;
    }

    public static List<Integer> getListOfInts(ResultSet resultSet, String columnName) {
        List<Integer> result = new LinkedList<>();

        try {
            while(resultSet.next()) {
                result.add(resultSet.getInt(columnName));
            }
        } catch (SQLException e) {
            e.getMessage();
            e.printStackTrace();
        }

        return result;
    }

    public static double getDouble(ResultSet resultSet, String columnName)
    {
        double result = -1;

        try {
            while(resultSet.next()) {
                result = resultSet.getDouble(columnName);
            }
        } catch (SQLException e) {
            e.getMessage();
            e.printStackTrace();
        }

        return result;
    }

    public static String getString(ResultSet resultSet, String columnName) {
        String result = "";

        try {
            while(resultSet.next()) {
                result = resultSet.getString(columnName);
            }
        } catch (SQLException e) {
            e.getMessage();
            e.printStackTrace();
        }

        return result;
    }
}
