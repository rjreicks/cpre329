package arkontrol.jdbc;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.Scanner;

/**
 * Created by davisjohnson on 2/16/16
 */

public class JDBChelper{

    /**
     * Attemps to connect to the database mysql.cs.iastate.edu
     *
     * @return Connection Object
     */
    public static Connection connectToDB() {
        Connection conn1 = null;
        String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db309grp13";
        String user = "dbu309grp13";
        String password = "sFQmaWV6KBH";

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception E) {
            System.err.println("Unable to load driver");
        }

        //System.out.print("Connecting to database " + dbUrl + " with username " + user + "...");

        try {
            conn1 = DriverManager.getConnection(dbUrl, user, password);
            conn1.setAutoCommit(false);
            //System.out.println("success!");
        } catch (SQLException E) {
            System.out.println("SQL Exception: " + E.getMessage());
            System.out.println("SQL State: " + E.getSQLState());
            System.out.println("VendorError: " + E.getErrorCode());
        }

        return conn1;
    }

    /**
     * Attempts to connect to the database specifed by conString. Prompts for username and password.
     *
     * @param conString String containing database path. Should be of the format:
     *                  "jdbc:mysql://[database server]:[port]/[database name]"
     * @return Connection Object
     */
    public static Connection connectToDB(String conString) {

        Connection conn1 = null;
        Scanner reader = new Scanner(System.in);
        String username = null;
        String password = null;

        //System.out.println("Attempting to connect to database...");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception E) {
            System.err.println("Unable to load driver");
        }

        System.out.println("Please enter your username: ");
        username = reader.next();

        System.out.println("Please enter your password: ");
        password = reader.next();
        //System.out.print("Connecting to database...");

        try {
            conn1 = DriverManager.getConnection(conString, username, password);
            conn1.setAutoCommit(false);
            //System.out.println("success!");
        } catch (SQLException E) {
            System.out.println("SQL Exception: " + E.getMessage());
            System.out.println("SQL State: " + E.getSQLState());
            System.out.println("VendorError: " + E.getErrorCode());
        }

        reader.close();

        return conn1;

    }

    /**
     * Attempts to connect to the database specifed by conString using username and password as credentials
     *
     * @param conString String containing database path. Should be of the format:
     *                  "jdbc:mysql://[database server]:[port]/[database name]"
     * @param username  Username string
     * @param password  Password string
     * @return Connection Object
     */
    public static Connection connectToDB(String conString, String username, String password) {

        Connection conn1 = null;

        //System.out.print("Attempting to connect to database...");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception E) {
            System.err.println("Unable to load driver");
        }

        try {
            conn1 = DriverManager.getConnection(conString, username, password);
            conn1.setAutoCommit(false);
            //System.out.println("success!");
            //System.out.println();

        } catch (SQLException E) {
            System.out.println("SQL Exception: " + E.getMessage());
            System.out.println("SQL State: " + E.getSQLState());
            System.out.println("VendorError: " + E.getErrorCode());
        }

        return conn1;
    }

    /**
     * Attempts to execute a SQL query and returns a ResultSet
     *
     * @param conn Connection Object for the query to be executed on
     * @param sql  SQL Query
     * @param stmt Statement object
     * @return ResultSet containing data from the SQL query
     */
    public static ResultSet executeQuery(Connection conn, String sql, Statement stmt) {

        stmt = null;

        try {
            stmt = conn.createStatement();
        } catch (SQLException E) {
            System.out.println("Statement creation failed");
        }

        ResultSet rs = null;

        try {
            //System.out.print("Attempting to execute SQL query...");
            rs = stmt.executeQuery(sql);
            //System.out.println("success!");
        } catch (SQLException E) {
            System.out.println("Query execution failed with error: " + E.getMessage());
        }


        return rs;
    }

    /**
     * Attempts to execute a SQL update
     *
     * @param conn Connection Object for the update to be executed on
     * @param sql  SQL Update
     * @param stmt Statement object
     */
    public static void executeUpdate(Connection conn, String sql, Statement stmt) {

        try {
            stmt = conn.createStatement();
        } catch (SQLException E) {
            System.out.println("Statement creation failed");
        }

        try {
            //System.out.print("Attempting to execute SQL update...");
            stmt.executeUpdate(sql);
            //System.out.println("success!");
        } catch (SQLException E) {
            System.out.println("Update execution failed with error: ");
            System.out.println(E.getMessage());
        }

    }

    /**
     * Attempts to execute a SQL statement
     *
     * @param conn Connection Object for the statement to be executed on
     * @param sql  SQL Statement
     * @param stmt Statement object
     */
    public static void execute(Connection conn, String sql, Statement stmt) {

        try {
            stmt = conn.createStatement();
        } catch (SQLException E) {
            System.out.println("Statement creation failed");
        }

        try {
            //System.out.print("Attempting to execute SQL statement...");
            stmt.execute(sql);
            //System.out.println("success!");
        } catch (SQLException E) {
            System.out.println("Statement execution failed with error: ");
            System.out.println(E.getMessage());
        }

    }

    /**
     * Attempts to close all open JDBC resources in the following order: ResultSet,Statement,Connection
     *
     * @param conn Connection object to close
     * @param stmt Statement object to close
     * @param rs   ResultSet object to close
     */
    public static void closeResources(Connection conn, Statement stmt, ResultSet rs) {
        //System.out.print("Attempting to close resources...");

        //Commit changes to database
        try {
            conn.commit();
        } catch (SQLException e) {
            System.out.println("Error committing changes:");
            e.getMessage();
        }
        //Close the ResultSet
        try {
            if (rs != null)
                rs.close();
        } catch (SQLException e) {
            System.out.println("Error closing ResultSet");
            e.getMessage();
        }
        //close Statement
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Error closing Statement");
            e.getMessage();
        }
        //close the DB connection
        try {
            if (conn != null) {
                conn.close();
                //System.out.println("success!");
            }
        } catch (SQLException e) {
            System.out.println("Connection closure failure");
            e.getMessage();
        }
    }

    /**
     * Gets a user's email from the database
     *
     * @param conn     Connection object
     * @param username User's username
     * @return The users email
     */
    public static String getEmail(Connection conn, String username) {
        String email = null;
        PreparedStatement getEmail;
        String emailQuery = "SELECT email\n" +
                "FROM db309grp13.newUsers\n" +
                "WHERE username = ?";
        ResultSet rs;

        try {
            getEmail = conn.prepareStatement(emailQuery);
            getEmail.setString(1, username);
            rs = getEmail.executeQuery();
            while (rs.next()) {
                email = rs.getString("email");
            }
            rs.close();
            getEmail.close();
        } catch (SQLException e) {
            e.getMessage();
        }


        return email;
    }

    /**
     * Returns the database ID of the user specified. If the user is not found, will either error or return -1
     * @param conn Connection object
     * @param username username for the userID in question
     * @return User's ID, otherwise -1
     */
    public static int getUserID(Connection conn, String username) {
        int userID = -1;
        PreparedStatement getUserID;
        String userIDquery = "SELECT userid \n" +
                "FROM db309grp13.newUsers\n" +
                "WHERE username = ?";
        ResultSet rs;

        try {
            getUserID = conn.prepareStatement(userIDquery);
            getUserID.setString(1, username);
            rs = getUserID.executeQuery();
            while (rs.next()) {
                userID = rs.getInt("userid");
            }
        } catch (SQLException e) {
            e.getMessage();
        }

        return userID;
    }

    public static boolean getTwoFactorAuthBool(String username){

        Connection conn = connectToDB();
        boolean result = false;
        int temp = 0;
        PreparedStatement getTFAbool;
        String getTFAboolSQL = "SELECT twoFactor \n" +
                "FROM db309grp13.newUsers\n" +
                "WHERE username = ?";

        ResultSet rs;

        try {
            getTFAbool = conn.prepareStatement(getTFAboolSQL);
            getTFAbool.setString(1, username);
            rs = getTFAbool.executeQuery();
            while (rs.next()) {
                temp = rs.getInt("twoFactor");
                if(temp>0) {
                    result = true;
                }
            }
        } catch (SQLException e) {
            e.getMessage();
            return false;
        }
        try {
            getTFAbool.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeResources(conn,null,rs);

        return result;
    }

    public static boolean isUser(Connection conn, String username){

        boolean result = false;
        PreparedStatement isUser;
        String isUserSQL = "SELECT username \n" +
                "FROM db309grp13.newUsers\n" +
                "WHERE username = ?";

        ResultSet rs;

        try {
            isUser = conn.prepareStatement(isUserSQL);
            isUser.setString(1, username);
            rs = isUser.executeQuery();
            if(rs.next()){
                result = true;
            }
        } catch (SQLException e) {
            e.getMessage();
            return false;
        }

        return result;
    }

    /**
     * Moves any TFA entries for the specified user from the TFA table to the inactive TFA table
     * @param conn
     * @param username
     */
    public static void moveTFAcode(Connection conn, String username){
        int userID = getUserID(conn,username);
        String moveTFAcodeSQL = "INSERT INTO inactiveTFAcodes (User_ID,TFA_Code,Creation_Time)\n" +
                "SELECT User_ID,TFA_Code,Creation_Time\n" +
                "FROM twoFactorAuth\n" +
                "WHERE User_ID = '" + userID +"';\n";

        execute(conn,moveTFAcodeSQL,null);

        moveTFAcodeSQL = "DELETE FROM twoFactorAuth\n" +
                "WHERE User_ID = '" + userID + "';";

        execute(conn,moveTFAcodeSQL,null);

    }

    public static boolean checkPassword(Connection conn, String username, String password) {
        return false;
    }

    public static int getTFAcode(Connection conn, String username){
        int result = -1;
        int temp;
        PreparedStatement getTFAcode;
        int userID = JDBChelper.getUserID(conn,username);
        String getTFAcodeSQL = "SELECT TFA_Code \n" +
                "FROM twoFactorAuth \n" +
                "WHERE User_ID = ?";

        ResultSet rs;

        try {
            getTFAcode = conn.prepareStatement(getTFAcodeSQL);
            getTFAcode.setString(1, Integer.toString(userID));
            rs = getTFAcode.executeQuery();
            while (rs.next()) {
                temp = rs.getInt("TFA_Code");
                if(temp>0) {
                    result = temp;
                }
            }
        } catch (SQLException e) {
            e.getMessage();
            return result;
        }

        return result;
    }

}
