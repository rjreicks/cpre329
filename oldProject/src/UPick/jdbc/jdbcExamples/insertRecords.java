package arkontrol.jdbc.jdbcExamples;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import arkontrol.jdbc.JDBChelper;

/**
 * Created by davisjohnson on 2/16/16.
 */
public class insertRecords {

    public static void main(String[] args){

        final String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db363davisj";
        final String user = "dbu363davisj";
        final String password = "AnRd7yR3LMy";
        Statement stmt = null;
        //ResultSet rs = null;
        Connection conn;

        String[] insertArray = new String[42];

        String insertRecords1 = "INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1001','65465234','Randy','M','2000/12/01','301 E Hall','5152700988','121 Main','7083066321');\n";

        String insertRecords2 = "INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1002','123097834','Victor','M','2000/05/06','270 W Hall','5151234578','702 Walnut','7083066333');\n";

        String insertRecords3 ="INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1003','978012431','John','M','1999/07/08','201 W Hall','5154132805','888 University','5152012011');\n";

        String insertRecords4 = "INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1004','746897816','Seth','M','1998/12/30','199 N Hall','5158891504','21 Green','5154132907');\n";

        String insertRecords5 = "INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1005','186032894','Nicole','F','2001/04/01','178 S Hall','5158891155','13 Gray','5157162071');\n";

        String insertRecords6 = "INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1006','534218752','Becky','F','2001/05/16','12 N Hall','5157083698','189 Clark','2034367632');\n";

        String insertRecords7 = "INSERT INTO students \n" +
                "(snum,ssn,name,gender,dob,c_addr,c_phone,p_addr,p_phone)\n" +
                "VALUES \n" +
                "('1007','432609519','Kevin','M','2000/08/12','75 E Hall','5157082497','89 National','7182340772');\n";

        String insertRecords8 = "INSERT INTO departments \n" +
                "(code, name, phone, college)\n" +
                "VALUES \n" +
                "('401','Computer Science','5152982801','LAS');\n";

        String insertRecords9 = "INSERT INTO departments \n" +
                "(code, name, phone, college)\n" +
                "VALUES \n" +
                "('402','Mathematics','5152982802','LAS');\n";

        String insertRecords10 = "INSERT INTO departments \n" +
                "(code, name, phone, college)\n" +
                "VALUES \n" +
                "('403','Chemical Engineering','5152982803','Engineering');\n";

        String insertRecords11 = "INSERT INTO departments \n" +
                "(code, name, phone, college)\n" +
                "VALUES \n" +
                "('404','Landscape Architect','5152982804','Design');\n";

        String insertRecords12 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Computer Science','BS','401');\n";

        String insertRecords13 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Software Engineering','BS','401');\n";

        String insertRecords42 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Computer Science','MS','401');\n";

        String insertRecords14 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Computer Science','PhD','401');\n";

        String insertRecords15 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Applied Mathematics','MS','402');\n";

        String insertRecords16 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Chemical Engineering','BS','403');\n";

        String insertRecords17 = "INSERT INTO degrees \n" +
                "(name, level, department_code)\n" +
                "VALUES \n" +
                "('Landscape Architect','BS','404');\n";

        String insertRecords18 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1001','Computer Science','BS');\n";

        String insertRecords19 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1002','Software Engineering','BS');\n";

        String insertRecords20 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1003','Chemical Engineering','BS');\n";

        String insertRecords21 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1004','Landscape Architect','BS');\n";

        String insertRecords22 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1005','Computer Science','MS');\n";

        String insertRecords23 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1006','Applied Mathematics','MS');\n";

        String insertRecords24 = "INSERT INTO major \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1007','Computer Science','PhD');\n";

        String insertRecords25 = "INSERT INTO minor \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1007','Applied Mathematics','MS');\n";

        String insertRecords26 = "INSERT INTO minor \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1005','Applied Mathematics','MS');\n";

        String insertRecords27 = "INSERT INTO minor \n" +
                "(snum, name, level)\n" +
                "VALUES \n" +
                "('1001','Software Engineering','BS');\n";

        String insertRecords28 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('113','Spreadsheet','Microsoft Excel and Access','3','Undergraduate','401');\n";

        String insertRecords29 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('311','Algorithm','Design and Analysis','3','Undergraduate','401');\n";

        String insertRecords30 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('531','Theory of Computation','Theorem and Probability','3','Graduate','401');\n";

        String insertRecords31 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('363','Database','Design Principle','3','Undergraduate','401');\n";

        String insertRecords32 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('412','Water Management','Water Management','3','Undergraduate','404');\n";

        String insertRecords33 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('228','Special Topics','Interesting Topics about CE','3','Undergraduate','403');\n";

        String insertRecords34 = "INSERT INTO courses \n" +
                "(number,name,description,credithours,level,department_code)\n" +
                "VALUES \n" +
                "('101','Calculus','Limit and Derivative','4','Undergraduate','402');\n";

        String insertRecords35 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1001','363','Fall2015','3');\n";

        String insertRecords36 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1002','311','Fall2015','4');\n";

        String insertRecords37 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1003','228','Fall2015','4');\n";

        String insertRecords38 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1004','363','Spring2015','3');\n";

        String insertRecords39 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1005','531','Spring2015','4');\n";

        String insertRecords40 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1006','363','Fall2015','3');\n";

        String insertRecords41 = "INSERT INTO register \n" +
                "(snum,course_number,`when`,grade)\n" +
                "VALUES \n" +
                "('1007','531','Spring2015','4');";




        conn = JDBChelper.connectToDB(dbUrl,user,password);

        System.out.println();

        JDBChelper.executeUpdate(conn,insertRecords1,stmt);
        JDBChelper.executeUpdate(conn,insertRecords2,stmt);
        JDBChelper.executeUpdate(conn,insertRecords3,stmt);
        JDBChelper.executeUpdate(conn,insertRecords4,stmt);
        JDBChelper.executeUpdate(conn,insertRecords5,stmt);
        JDBChelper.executeUpdate(conn,insertRecords6,stmt);
        JDBChelper.executeUpdate(conn,insertRecords7,stmt);
        JDBChelper.executeUpdate(conn,insertRecords8,stmt);
        JDBChelper.executeUpdate(conn,insertRecords9,stmt);
        JDBChelper.executeUpdate(conn,insertRecords10,stmt);
        JDBChelper.executeUpdate(conn,insertRecords11,stmt);
        JDBChelper.executeUpdate(conn,insertRecords12,stmt);
        JDBChelper.executeUpdate(conn,insertRecords13,stmt);
        JDBChelper.executeUpdate(conn,insertRecords42,stmt);
        JDBChelper.executeUpdate(conn,insertRecords14,stmt);
        JDBChelper.executeUpdate(conn,insertRecords15,stmt);
        JDBChelper.executeUpdate(conn,insertRecords16,stmt);
        JDBChelper.executeUpdate(conn,insertRecords17,stmt);
        JDBChelper.executeUpdate(conn,insertRecords18,stmt);
        JDBChelper.executeUpdate(conn,insertRecords19,stmt);
        JDBChelper.executeUpdate(conn,insertRecords20,stmt);
        JDBChelper.executeUpdate(conn,insertRecords21,stmt);
        JDBChelper.executeUpdate(conn,insertRecords22,stmt);
        JDBChelper.executeUpdate(conn,insertRecords23,stmt);
        JDBChelper.executeUpdate(conn,insertRecords24,stmt);
        JDBChelper.executeUpdate(conn,insertRecords25,stmt);
        JDBChelper.executeUpdate(conn,insertRecords26,stmt);
        JDBChelper.executeUpdate(conn,insertRecords27,stmt);
        JDBChelper.executeUpdate(conn,insertRecords28,stmt);
        JDBChelper.executeUpdate(conn,insertRecords29,stmt);
        JDBChelper.executeUpdate(conn,insertRecords30,stmt);
        JDBChelper.executeUpdate(conn,insertRecords31,stmt);
        JDBChelper.executeUpdate(conn,insertRecords32,stmt);
        JDBChelper.executeUpdate(conn,insertRecords33,stmt);
        JDBChelper.executeUpdate(conn,insertRecords34,stmt);
        JDBChelper.executeUpdate(conn,insertRecords35,stmt);
        JDBChelper.executeUpdate(conn,insertRecords36,stmt);
        JDBChelper.executeUpdate(conn,insertRecords37,stmt);
        JDBChelper.executeUpdate(conn,insertRecords38,stmt);
        JDBChelper.executeUpdate(conn,insertRecords39,stmt);
        JDBChelper.executeUpdate(conn,insertRecords40,stmt);
        JDBChelper.executeUpdate(conn,insertRecords41,stmt);


        System.out.println();

        JDBChelper.closeResources(conn,stmt,null);

    }

}
