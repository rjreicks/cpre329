package arkontrol.jdbc.jdbcExamples;

import java.sql.*;
import java.util.Scanner;
import arkontrol.jdbc.JDBChelper;

/**
 * Created by davisjohnson on 2/15/16.
 */

public class createTables {

    public static void main (String args[]){

        final String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db363davisj";
        final String user = "dbu363davisj";
        final String password = "AnRd7yR3LMy";
        Statement stmt = null;
        //ResultSet rs = null;
        Connection conn;


        String createDepartments = "CREATE TABLE departments(\n" +
                "\tcode INTEGER NOT NULL,\n" +
                "\tname VARCHAR(50),\n" +
                "\tphone VARCHAR(10),\n" +
                "\tcollege VARCHAR(20),\n" +
                "\tPRIMARY KEY (code),\n" +
                "\tUNIQUE (name)\n" +
                "\t);";

        String createStudents = "CREATE TABLE students(\n" +
                "\tsnum INTEGER NOT NULL,\n" +
                "\tssn INTEGER NOT NULL,\n" +
                "\tname VARCHAR(10),\n" +
                "\tgender VARCHAR(1),\n" +
                "\tdob DATETIME,\n" +
                "\tc_addr VARCHAR(20),\n" +
                "\tc_phone VARCHAR(10),\n" +
                "\tp_addr VARCHAR(20),\n" +
                "\tp_phone VARCHAR(10),\n" +
                "\tPRIMARY KEY ( ssn ),\n" +
                "\tUNIQUE (snum)\n" +
                "\t);";

        String createDegrees = "CREATE TABLE degrees(\n" +
                "\tname VARCHAR(50) NOT NULL,\n" +
                "\tlevel VARCHAR(5) NOT NULL,\n" +
                "\tdepartment_code INTEGER,\n" +
                "\tPRIMARY KEY (name, level),\n" +
                "\tFOREIGN KEY (department_code) REFERENCES departments(code)\n" +
                "\t);";

        String createCourses = "CREATE TABLE courses(\n" +
                "\tnumber INTEGER NOT NULL,\n" +
                "\tname VARCHAR(50) UNIQUE,\n" +
                "\tdescription VARCHAR(50),\n" +
                "\tcredithours INTEGER,\n" +
                "\tlevel VARCHAR(20),\n" +
                "\tdepartment_code INTEGER,\n" +
                "\tPRIMARY KEY (number),\n" +
                "\tFOREIGN KEY (department_code) REFERENCES departments(code)\n" +
                "\t);\n";

        String createMajor = "CREATE TABLE major(\n" +
                "\tsnum INTEGER NOT NULL,\n" +
                "\tname VARCHAR(50),\n" +
                "\tlevel VARCHAR(5),\n" +
                "\tPRIMARY KEY (snum,name,level),\n" +
                "\tFOREIGN KEY (snum) REFERENCES students(snum),\n" +
                "\tFOREIGN KEY (name, level) REFERENCES degrees(name,level)\n" +
                "\t);";

        String createMinor = "CREATE TABLE minor(\n" +
                "\tsnum INTEGER NOT NULL,\n" +
                "\tname VARCHAR(50),\n" +
                "\tlevel VARCHAR(5),\n" +
                "\tPRIMARY KEY (snum,name,level),\n" +
                "\tFOREIGN KEY (snum) REFERENCES students(snum),\n" +
                "\tFOREIGN KEY (name,level) REFERENCES degrees(name,level)\n" +
                "\t);";

        String createRegister = "CREATE TABLE register(\n" +
                "\tsnum INTEGER NOT NULL,\n" +
                "\tcourse_number INTEGER,\n" +
                "\t`when` VARCHAR(20),\n" +
                "\tgrade INTEGER,\n" +
                "\tPRIMARY KEY (snum, course_number),\n" +
                "\tFOREIGN KEY (snum) REFERENCES students(snum),\n" +
                "\tFOREIGN KEY (course_number) REFERENCES courses(number)\n" +
                "\t);";

        conn = JDBChelper.connectToDB(dbUrl,user,password);

        System.out.println();

        JDBChelper.execute(conn, createDepartments, stmt);
        JDBChelper.execute(conn, createStudents, stmt);
        JDBChelper.execute(conn, createDegrees, stmt);
        JDBChelper.execute(conn, createCourses, stmt);
        JDBChelper.execute(conn, createMajor, stmt);
        JDBChelper.execute(conn, createMinor, stmt);
        JDBChelper.execute(conn, createRegister, stmt);

        System.out.println();

        JDBChelper.closeResources(conn,stmt,null);
    }

}
