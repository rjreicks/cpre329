package arkontrol.jdbc.jdbcExamples;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import arkontrol.jdbc.JDBChelper;

/**
 * Created by davisjohnson on 2/16/16.
 */
public class modifyRecords {

    public static void main(String[] args){

        final String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db363davisj";
        final String user = "dbu363davisj";
        final String password = "AnRd7yR3LMy";
        Statement stmt = null;
        String modifyRecord = "";
        Connection conn;

        conn = JDBChelper.connectToDB(dbUrl,user,password);

        modifyRecord = "UPDATE students\n" +
                "SET students.name = \"Scott\"\n" +
                "WHERE students.ssn = \"746897816\";";

        JDBChelper.executeUpdate(conn,modifyRecord,stmt);

        modifyRecord = "UPDATE major\n" +
                "SET major.name = \"Computer Science\", major.level = \"MS\"\n" +
                "WHERE major.snum = \"1004\";";

        JDBChelper.executeUpdate(conn,modifyRecord,stmt);

        modifyRecord = "DELETE FROM register\n" +
                "WHERE register.`when`=\"Spring2015\";";

        JDBChelper.executeUpdate(conn,modifyRecord,stmt);

        JDBChelper.closeResources(conn,stmt,null);
    }

}
