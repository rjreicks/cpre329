package arkontrol.jdbc.jdbcExamples;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import arkontrol.jdbc.JDBChelper;

/**
 * Created by davisjohnson on 2/16/16.
 */
public class query {

    public static void main(String[] args){

        final String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db363davisj";
        final String user = "dbu363davisj";
        final String password = "AnRd7yR3LMy";
        Statement stmt = null;
        ResultSet rs = null;
        String query = "";
        String queryResult = "";
        Connection conn;


        conn = JDBChelper.connectToDB(dbUrl,user,password);

        System.out.println();

        query = "SELECT snum, ssn\n" +
                "FROM students\n" +
                "WHERE name=\"Becky\";";

        rs = JDBChelper.executeQuery(conn,query,stmt);

        try {
            while (rs.next()) {
                queryResult += rs.getInt("snum") + "\n";
                queryResult += rs.getInt("ssn");
            }
        }
        catch(SQLException e){
            System.out.println("Printing failed with error: ");
            System.out.println(e.getMessage());
        }

        System.out.println(queryResult);
        queryResult = "";
        System.out.println();

        query = "SELECT major.name, major.level\n" +
                "FROM major\n" +
                "INNER JOIN students\n" +
                "ON students.snum=major.snum\n" +
                "WHERE students.ssn=\"123097834\";";

        rs = JDBChelper.executeQuery(conn,query,stmt);

        try {
            while (rs.next()) {
                queryResult += rs.getString("name") + "\n";
                queryResult += rs.getString("level");
            }
        }
        catch(SQLException e){
            System.out.println("Printing failed with error: ");
            System.out.println(e.getMessage());
        }

        System.out.println(queryResult);
        queryResult = "";
        System.out.println();

        query = "SELECT courses.name\n" +
                "FROM courses\n" +
                "INNER JOIN departments\n" +
                "ON courses.department_code=departments.code\n" +
                "WHERE departments.code=\"401\";";

        rs = JDBChelper.executeQuery(conn,query,stmt);

        try {
            while (rs.next()) {
                queryResult += rs.getString("name") + "\n";
            }
        }
        catch(SQLException e){
            System.out.println("Printing failed with error: ");
            System.out.println(e.getMessage());
        }
        System.out.println(queryResult);
        queryResult = "";
        System.out.println();

        query = "SELECT name,level\n" +
                "FROM degrees\n" +
                "WHERE department_code = \"401\";";

        rs = JDBChelper.executeQuery(conn,query,stmt);

        try {
            while (rs.next()) {
                queryResult += rs.getString("name") + "...";
                queryResult += rs.getString("level") + "\n";
            }
        }
        catch(SQLException e){
            System.out.println("Printing failed with error: ");
            System.out.println(e.getMessage());
        }
        System.out.println(queryResult);
        queryResult = "";
        System.out.println();

        query = "SELECT students.name\n" +
                "FROM students\n" +
                "INNER JOIN minor\n" +
                "ON students.snum=minor.snum;";

        rs = JDBChelper.executeQuery(conn,query,stmt);

        try {
            while (rs.next())
                queryResult += rs.getString("name") + "\n";
        }
        catch(SQLException e){
            System.out.println("Printing failed with error: ");
            System.out.println(e.getMessage());
        }
        System.out.println(queryResult);

        JDBChelper.closeResources(conn,stmt,rs);
    }

}
