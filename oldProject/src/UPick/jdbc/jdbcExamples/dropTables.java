package arkontrol.jdbc.jdbcExamples;

import java.sql.*;
import arkontrol.jdbc.JDBChelper;

/**
 * Created by davisjohnson on 2/16/16.
 */
public class dropTables {

    public static void main(String args[]) {

        final String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db363davisj";
        final String user = "dbu363davisj";
        final String password = "AnRd7yR3LMy";
        Statement stmt = null;
       // ResultSet rs = null;
        Connection conn;

        String dropTables = "DROP TABLE register;";
        String dropCourses = "DROP TABLE courses;";
        String dropMajor = "DROP TABLE major;";
        String dropMinor = "DROP TABLE minor;";
        String dropDegrees = "DROP TABLE degrees;";
        String dropStudents = "DROP TABLE students;";
        String dropDepartments = "DROP TABLE departments;";

        conn = JDBChelper.connectToDB(dbUrl,user,password);

        System.out.println();

        JDBChelper.execute(conn,dropTables,stmt);
        JDBChelper.execute(conn,dropCourses,stmt);
        JDBChelper.execute(conn,dropMajor,stmt);
        JDBChelper.execute(conn,dropMinor,stmt);
        JDBChelper.execute(conn,dropDegrees,stmt);
        JDBChelper.execute(conn,dropStudents,stmt);
        JDBChelper.execute(conn,dropDepartments,stmt);

        System.out.println();

        JDBChelper.closeResources(conn,stmt,null);

    }

}