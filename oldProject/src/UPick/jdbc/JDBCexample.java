package arkontrol.jdbc;
import java.sql.*;

/**
 * Created by davisjohnson on 2/18/16.
 * Refer to jdbcExamples for more thorough examples, this example is just specific to our database.
 */
public class JDBCexample {

    public static void main(String[] args){

        String dbUrl = "jdbc:mysql://mysql.cs.iastate.edu:3306/db309grp13";
        String user = "dbu309grp13";
        String password = "sFQmaWV6KBH";
        Statement stmt = null;
        Connection conn;
        String sql;

        //insert SQL
        sql = "";

        conn = JDBChelper.connectToDB(dbUrl,user,password);

        JDBChelper.execute(conn,sql,stmt);

        JDBChelper.closeResources(conn,stmt,null);

    }
}
