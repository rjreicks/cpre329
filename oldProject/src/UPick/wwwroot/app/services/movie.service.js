"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
var MovieService = (function () {
    function MovieService(http) {
        this.http = http;
    }
    ;
    MovieService.prototype.getMoviesByGenreId = function (id) {
        return this.http.get('/api/movies/genre/' + id).map(function (movies) { return movies.json(); });
    };
    return MovieService;
}());
MovieService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MovieService);
exports.MovieService = MovieService;
var Movie = (function () {
    function Movie(id, title, overview, IMDBId, budget, language, releaseDate, revenue, runtime, status, tagline) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.IMDBId = IMDBId;
        this.budget = budget;
        this.language = language;
        this.releaseDate = releaseDate;
        this.revenue = revenue;
        this.runtime = runtime;
        this.status = status;
        this.tagline = tagline;
    }
    return Movie;
}());
exports.Movie = Movie;
//# sourceMappingURL=movie.service.js.map