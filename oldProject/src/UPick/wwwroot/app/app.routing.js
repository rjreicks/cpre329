"use strict";
var router_1 = require("@angular/router");
var genres_component_1 = require("./genres/genres.component");
var movies_component_1 = require("./movie/movies.component");
var appRoutes = [
    { path: '', redirectTo: '/genres', pathMatch: 'full' },
    { path: 'movies/:id', component: movies_component_1.MoviesComponent },
    { path: 'genres', component: genres_component_1.GenresComponent, pathMatch: 'full' }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map