"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var genre_service_1 = require("../services/genre.service");
var router_1 = require("@angular/router");
var GenresComponent = (function () {
    function GenresComponent(GenService, router) {
        this.GenService = GenService;
        this.router = router;
        this.name = 'Genres';
    }
    GenresComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.GenService.getAll().subscribe(function (genres) { return _this.genres = genres; });
    };
    GenresComponent.prototype.getMoviesByCategory = function (id) {
        this.router.navigate(['/movies', id]);
    };
    return GenresComponent;
}());
GenresComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'genres',
        template: "<div class=\"page-title col-sm-12\">\n                    <h4>Genres:</h4>\n                </div>\n                <div class=\"genre-list\">\n                    <span *ngFor=\"let genre of genres\">\n                        <button class=\"btn genre\" [routerLink]=\"['/movies', genre.id]\">{{genre.id}}</button>\n                    </span>\n                </div>",
        styleUrls: ['genres.component.css']
    }),
    __metadata("design:paramtypes", [genre_service_1.GenreService,
        router_1.Router])
], GenresComponent);
exports.GenresComponent = GenresComponent;
//# sourceMappingURL=genres.component.js.map