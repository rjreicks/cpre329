﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieComponent } from './movie/movie.component';
import { GenresComponent } from './genres/genres.component';
import { MoviesComponent } from './movie/movies.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/genres', pathMatch: 'full' },
    { path: 'movies/:id', component: MoviesComponent },
    { path: 'genres', component: GenresComponent, pathMatch: 'full' }
]

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);