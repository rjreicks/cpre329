﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { MovieService, Movie } from '../services/movie.service';

import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    selector: 'movie',
    templateUrl: 'movie.component.html',
    styleUrls: ['movie.component.css']
})
export class MovieComponent implements OnInit {
    movie: Movie;
    constructor(
        private MovService: MovieService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit() {

    }
}