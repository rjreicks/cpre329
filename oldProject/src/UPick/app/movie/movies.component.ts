﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { MovieService, Movie } from '../services/movie.service';

import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    selector: 'movies',
    templateUrl: 'movies.component.html'
})
export class MoviesComponent implements OnInit {
    movies: Movie[];
    constructor(
        private MovService: MovieService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit() {
        this.route.params
            .switchMap((params: Params) => this.MovService.getMoviesByGenreId(+params['id']))
            .subscribe(x => this.movies = x);
    }
}