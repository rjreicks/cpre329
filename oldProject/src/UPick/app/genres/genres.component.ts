﻿import { Component, OnInit } from '@angular/core';
import { GenreService, Genre } from '../services/genre.service';
import { MovieService, Movie } from '../services/movie.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'genres',
    template: `<div class="page-title col-sm-12">
                    <h4>Genres:</h4>
                </div>
                <div class="genre-list">
                    <span *ngFor="let genre of genres">
                        <button class="btn genre" [routerLink]="['/movies', genre.id]">{{genre.id}}</button>
                    </span>
                </div>`,
    styleUrls: ['genres.component.css']
})
export class GenresComponent implements OnInit {
    name = 'Genres';
    constructor(
        private GenService: GenreService,
        private router: Router
    ) { }

    genres: Genre[];

    ngOnInit() {
        this.GenService.getAll().subscribe(genres => this.genres = genres);
    }

    getMoviesByCategory(id: number) {
        this.router.navigate(['/movies', id]);
    }
}