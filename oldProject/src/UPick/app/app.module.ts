﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MovieComponent } from './movie/movie.component';
import { MoviesComponent } from './movie/movies.component';
import { GenresComponent } from './genres/genres.component';

import { GenreService } from './services/genre.service';
import { MovieService } from './services/movie.service';
import { routing, appRoutingProviders } from './app.routing';

@NgModule({
    imports: [BrowserModule, HttpModule, JsonpModule, routing],
    declarations: [AppComponent, MovieComponent, GenresComponent, MoviesComponent],
    providers: [GenreService, MovieService, appRoutingProviders],
    bootstrap: [AppComponent]
})
export class AppModule { }