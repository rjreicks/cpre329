﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class MovieService {

    constructor(private http: Http) { };

    getMoviesByGenreId(id: number): Observable<Movie[]> {
        return this.http.get('/api/movies/genre/' + id).map(movies => movies.json() as Movie[]);
    }
}

export class Movie {
    constructor(
        public id: number,
        public title: string,
        public overview: string,
        public IMDBId: string,
        public budget: number,
        public language: string,
        public releaseDate: string,
        public revenue: string,
        public runtime: number,
        public status: string,
        public tagline: string) { }
}