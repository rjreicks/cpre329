﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class GenreService {

    constructor(private http: Http) { };

    getAll(): Observable<Genre[]> {
        return this.http.get('/api/genres').map(genres => genres.json() as Genre[]);
    }
}

export class Genre {
    constructor(public id: number, public name: string)
    {

    }
}