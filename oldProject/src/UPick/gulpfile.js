﻿/// <binding AfterBuild='move_html' ProjectOpened='move_node' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
    watch = require('gulp-watch');

gulp.task("move_node", function () {
    gulp.src([
        "node_modules/**/*"
    ])
        .pipe(gulp.dest("wwwroot/node_modules"));
});

gulp.task("move_html", function () {
    gulp.src([
        "./app/**/*.html",
        "./app/**/*.css"
    ], { base: './app/' })
        .pipe(gulp.dest("./wwwroot/app"));
})

gulp.task('watch_html', function () {
    // Callback mode, useful if any plugin in the pipeline depends on the `end`/`flush` event 
    return watch('./app/**/*.html', function () {
        gulp.src([
            "./app/**/*.html",
            "./app/**/*.css"
        ], { base: './app/' })
            .pipe(gulp.dest("./wwwroot/app"));
    });
});

gulp.watch("./app/**/*.html", ['move_html']);