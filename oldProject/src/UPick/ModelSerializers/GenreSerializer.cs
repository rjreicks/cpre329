﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UPick.ApiModels;
using UPick.Models;

namespace UPick.ModelSerializers
{
    public class GenreSerializer : SerializerBase<Genre, GenreApi>
    {
        public override GenreApi ToApi(Genre model)
        {
            return new GenreApi() {
                id = model.Id,
                name = model.Name
            };
        }
    }
}
