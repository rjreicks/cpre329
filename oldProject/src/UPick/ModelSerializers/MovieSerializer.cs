﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UPick.Models;
using UPick.Models.ApiModels;

namespace UPick.ModelSerializers
{
    public class MovieSerializer : SerializerBase<Movie, MovieApi>
    {
        public override MovieApi ToApi(Movie model)
        {
            return new MovieApi()
            {
                Id = model.Id,
                IMDBId = model.IMDBId,
                Budget = model.Budget,
                Language = model.Language,
                Overview = model.Overview,
                ReleaseDate = model.ReleaseDate,
                Revenue = model.Revenue,
                Runtime = model.Runtime,
                Status = model.Status,
                Tagline = model.Tagline,
                Title = model.Title
            };
        }
    }
}
