﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPick.ModelSerializers
{
    public abstract class SerializerBase<TEntity, TAPIEntity>
    {
        public IEnumerable<TAPIEntity> ToApiList(IEnumerable<TEntity> models)
        {
            List<TAPIEntity> apiModels = new List<TAPIEntity>();
            foreach (var model in models)
            {
                apiModels.Add(ToApi(model));
            }
            return apiModels;
        }

        public abstract TAPIEntity ToApi(TEntity model);
    }
}
