﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UPick.Models;
using UPick.ModelSerializers;
using UPick.ApiModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace UPick.Controllers
{
    [Route("api/genres")]
    public class GenresApiController : Controller
    {
        private readonly UPickContext _context;
        private readonly GenreSerializer _serializer;
        public GenresApiController(UPickContext context)
        {
            _serializer = new GenreSerializer();
            _context = context;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<GenreApi> Get()
        {
            return _serializer.ToApiList(_context.Genres.ToList());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public GenreApi Get(int id)
        {
            return _serializer.ToApi(_context.Genres.FirstOrDefault(x => x.Id == id));
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Genre genre)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Genre genre)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
