﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UPick.Models;
using UPick.ModelSerializers;
using UPick.Models.ApiModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace UPick.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        private readonly UPickContext _context;
        private readonly MovieSerializer _serializer;
        public MoviesController(UPickContext context)
        {
            _serializer = new MovieSerializer();
            _context = context;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<MovieApi> Get()
        {
            return _serializer.ToApiList(_context.Movies);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public MovieApi Get(int id)
        {
            return _serializer.ToApi(_context.Movies.FirstOrDefault(x => x.Id == id));
        }

        // GET api/values/5
        [HttpGet("genre/{id}")]
        public IEnumerable<MovieApi> GetByGenreId(int id)
        {
            var lookups = _context.GenreLookups.Where(x => x.GenreId == id);
            return _serializer.ToApiList(lookups.Select(x => x.Movie));
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
