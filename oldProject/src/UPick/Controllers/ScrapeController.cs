﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using UPick.Models;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace UPick.Controllers
{
    public class ScrapeController : Controller
    {
        private readonly UPickContext _context;

        public ScrapeController(UPickContext context)
        {
            _context = context;
        }
        
        public async Task<string> Genres()
        {
            WebRequest req = WebRequest.Create("https://api.themoviedb.org/3/genre/movie/list?language=en-US&api_key=c3298457548fe4889463c598108cdf06");
            var res = await req.GetResponseAsync();
            var rs = res.GetResponseStream();
            var content = new StreamReader(rs, Encoding.UTF8).ReadToEnd();
            content = content.Remove(0, 10);
            content = content.Remove(content.Count()-1);
            var genres = JsonConvert.DeserializeObject<List<Genre>>(content);
            _context.Genres.AddRange(genres);
            await _context.SaveChangesAsync();

            return "Scraped data";
        }

        public async Task<string> Movies()
        {
            IEnumerable<Genre> genres = _context.Genres;
            HashSet<Movie> movies = new HashSet<Movie>();
            List<GenreLookup> lookups = new List<GenreLookup>();
            foreach (var genre in genres)
            {
                WebRequest req = WebRequest.Create("https://api.themoviedb.org/3/discover/movie?api_key=c3298457548fe4889463c598108cdf06&language=en-US&sort_by=popularity.desc&include_adult=true&include_video=false&with_genres=" + genre.Id);
                var res = await req.GetResponseAsync();
                var rs = res.GetResponseStream();
                var content = new StreamReader(rs, Encoding.UTF8).ReadToEnd();
                var pageResults = JsonConvert.DeserializeObject<MovieResponse>(content);
                var currentPage = 1;
                rs.Dispose();
                res.Dispose();
                while(currentPage < Math.Min(pageResults.total_pages, 5))
                {
                    WebRequest pageReq = WebRequest.Create("https://api.themoviedb.org/3/discover/movie?api_key=c3298457548fe4889463c598108cdf06&language=en-US&sort_by=popularity.desc&include_adult=true&include_video=false&with_genres=" + genre.Id + "&page=" + currentPage);
                    res = await pageReq.GetResponseAsync();
                    rs = res.GetResponseStream();
                    content = new StreamReader(rs, Encoding.UTF8).ReadToEnd();
                    pageResults = JsonConvert.DeserializeObject<MovieResponse>(content);
                    System.Threading.Thread.Sleep(1000);
                    foreach (var result in pageResults.results)
                    {
                        var movie = new Movie()
                        {
                            Id = result.id,
                            Overview = result.overview,
                            ReleaseDate = result.release_date,
                            Language = result.original_language,
                            Title = result.title
                        };
                        if(movies.Count(x => x.Id == result.id) == 0)
                        {
                            movies.Add(movie);
                        }
                        
                        lookups.Add(new GenreLookup() { MovieId = movie.Id, GenreId = genre.Id });
                    }
                    rs.Dispose();
                    res.Dispose();
                    currentPage++;
                }

            }
            _context.Movies.AddRange(movies.Distinct());
            _context.GenreLookups.AddRange(lookups.Distinct());
            _context.SaveChanges();
            return "Scraped movies";
        }
    }
}
