﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using UPick.Models;

namespace UPick.Migrations
{
    [DbContext(typeof(UPickContext))]
    partial class UPickContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("UPick.Models.Genre", b =>
                {
                    b.Property<int>("Id");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Genre");
                });

            modelBuilder.Entity("UPick.Models.GenreLookup", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("GenreId");

                    b.Property<int>("MovieId");

                    b.HasKey("Id");

                    b.HasIndex("GenreId");

                    b.HasIndex("MovieId");

                    b.ToTable("GenreLookup");
                });

            modelBuilder.Entity("UPick.Models.Movie", b =>
                {
                    b.Property<int>("Id");

                    b.Property<int>("APIId");

                    b.Property<long>("Budget");

                    b.Property<string>("IMDBId");

                    b.Property<string>("Language");

                    b.Property<string>("Overview");

                    b.Property<DateTime?>("ReleaseDate");

                    b.Property<long>("Revenue");

                    b.Property<int>("Runtime");

                    b.Property<string>("Status");

                    b.Property<string>("Tagline");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("Movie");
                });

            modelBuilder.Entity("UPick.Models.GenreLookup", b =>
                {
                    b.HasOne("UPick.Models.Genre", "Genre")
                        .WithMany("Movies")
                        .HasForeignKey("GenreId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("UPick.Models.Movie", "Movie")
                        .WithMany("Genres")
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
