﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace UPick.Models
{
    public class Movie
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Overview { get; set; }
        public int APIId { get; set; }
        public string IMDBId { get; set; }
        public virtual ICollection<GenreLookup> Genres { get; set; }
        public long Budget { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public long Revenue { get; set; }
        public int Runtime { get; set; }
        public string Status { get; set; }
        public string Tagline { get; set; }
    }
}
