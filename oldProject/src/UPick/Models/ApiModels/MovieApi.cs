﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPick.Models.ApiModels
{
    public class MovieApi
    {
        public int Id { get; set; }
        public string Overview { get; set; }
        public string IMDBId { get; set; }
        public long Budget { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public long Revenue { get; set; }
        public int Runtime { get; set; }
        public string Status { get; set; }
        public string Tagline { get; set; }
    }
}
