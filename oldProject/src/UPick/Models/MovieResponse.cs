﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPick.Models
{
    public class MovieResponse
    {
        public int page { get; set;  }
        public IEnumerable<MovieResult> results { get; set; }

        public int total_pages { get; set; }
        public int total_results { get; set; }
    }

    public class MovieResult
    {
        public int id { get; set; }
        public string original_title { get; set; }
        public string original_language { get; set; }
        public string title { get; set; }
        public double popularity { get; set; }
        public string overview { get; set; }
        public DateTime? release_date { get; set; }
        public IEnumerable<int> genre_ids { get; set; }
    }
}
