﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPick.Models
{
    public class GenreLookup
    {
        public int Id { get; set; }
        public int GenreId { get; set; }
        public int MovieId { get; set; }

        public virtual Genre Genre {get; set;}
        public virtual Movie Movie { get; set; }
    }
}
