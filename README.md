## Prerequisites
* Make sure you have node installed on your machine *** Must be at least version 6.10
* Run `node -v` to get the current node version. If node is an unknown command you do not have node installed.
* Follow the instructions <a href="https://howtonode.org/how-to-install-nodejs">here</a>: to install node
* This should also install npm. To confirm run `npm -v` to get the current version

## SETUP
* cd into a new directory where you want the project stored
* clone this repo
* cd into the newly created upick folder
* cd into the client folder
* run `npm install` to get all the project dependencies
* run `npm install -g @angular/cli` to install angular command line tools
* run `ng serve -o` to run the project and open it in the browser
* after this is done, open visual studio and open the UPick project

## Creating a new branch
We are going to try branch by feature for this project.
In order to be successfult in this quest, we will use Trello to manage our task list
* When you find something you want to work on (a feature), create a new trello ticket and assign it to yourself
* Create a branch with the nameing convention, yourName_featureName this way we can tell who created the branch and what the branch aims to accomplish.
* Once you have implemented your new feature. Push to the remote repo and create a new merge request. You can assign it to whomever you would like. I am usually a decent code reviewer.
* Once your code has been approved or at least confirmed that it does break anything, It can be merged into the master branch.

### Note:  I have locked down the master branch so no one can directly publish to it. This is a safety precaution.

## Documentation and Questions
The code base as it is probably wont make a lot of sence to those unfamiliar with ASP.net or Angular 2. I will be documenting my code that I have already pushed this week and writing down and thing that I think doesn't make sense. 
If you have any specific questions, ask it on the dev slack page.

### Our Trello Page can be found <a href="https://trello.com/b/xniXSeI0/cpre329">here</a>

## Folder Structure
The project can be pretty well devided into three parts: A web scraper, an API, and an angular app. I will go into more detail about each, but first I need to discuss MVC architecture. 
MVC or Mode View Controller is a commonly used programming paredigm utilitied by many programming frameworks. In our application, both our client and our server side utilize this paredigm.
Models are simple classes that define the outline of the data we use and minipulate. I like to think of them just as data. Most of our models will mimic a single row in our database. Such as a movie model will contain a title, a rating, and a duration. A user model will contain a name, a username and liked movies. 
Controllers are where all the data minipulation happens. If we want to change someone age, we do it in a controller. Most methods take in models, adjust models and then return them. They are the brains of the operation. 
Views are just what they sound like, they are the output that the user sees. Our web scraper and API does not really have a view besides a message that reads "Movies scrapped" etc. But our client side application is full of HTML markup that is displayed to the user.
I like to think of projects in layers. If a user wants to edit the database, they press a button on the view, which makes a request to the controller with an intent and the controller reads the intent, determines if this user has the correct privledges and if so, retreives the data from the database as a model and returns it the view. The view then foramats the data for the user in a neat way.
Now that you have a run down on MVC, we can get into our project.
## Web Scraper
Like I stated before about the web scrapper it doesnt have its own view file because the user does not interact with it but it does have its models and controller. The controller is located inside the root controllers folder with the title "ScrapeController".  You can read through the methods in this controller but they arent doing anything crazy. It is making requests to the Movie api and using a 3rd party json deserializer to convert the responce into responce models. The ScrapeGenres method converts the responce into Genre Models and the Scrape Movie method converts the response to a temporary movie responce model and then breaks the responce up into a list of Movie models and GenreLookup models. If you are fimiliar with relational databases and many to many relationships you will understand what a lookup table is. I needed to create one to map the many genres to the many movies. 
## API
The idea behind a RESTful api is pretty easy, you make http request to a given specific URL the webserver forwards the request to the correct controller. Currently all of our Api controllers are located in the controllers folder and they are the MoviesApiController and the GenreApiController. The format of these are decently straight forward they are just like methods you have seen before in java classes except they have a complete of annotations. 1 for the type of request it should be receiving: GET, POST, PUT, DELETE and another anotation for the url endpoint. The methods minipulate the Database context entitled _context and update or retreive from the database as needed. That is the basics of this part.
## Angular Client
An angular 2 app can be decently confusing. The entirety of the project lies in the app folder. We have a couple of very important things. 1) main.ts this is the main file, its only purpose is to setup the angular app. dont touch this file. 2) app.module.ts: this is the file that connects all other files. If you want to create a new component or service, you need to declare it in this file. 3) app.component.ts, .html, .css this is a basic component and it happens to be the first component that we load when we open the site. A component itself is a model view controller. the app.component.html acts as the view and the app.component.ts acts as the controller that binds them together. There is a lot to learn about angular 2 and how things kind of work. I would suggest watching a 30 minute video to get an idea of how they work. I would suggest this one: https://www.youtube.com/watch?v=-zW1zHqsdyc 4) next we have a services folder. Here we have a bunch of angular services that are used to make requests to our api and pass the data back to our controllers. 5) the rest of the folder are components. We currently have a genres component and a movies component. There is a ton more detail that I could go into about this part of the project but it would be better to just ask questions as they come to you. This can get kind of confusing.


