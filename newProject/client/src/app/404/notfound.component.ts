import { Component } from '@angular/core';

@Component({
 moduleId: module.id,
 selector: '404',
 templateUrl: './notfound.component.html'
})

export class PageNotFound { }
